(function(){
	'use strict';

	angular
		.module('starter')
		.factory('DateHour', DateHour);

	  	function DateHour($rootScope){
		   var dateHour = {};
		   
		   // variáveis para data e hora de Check In, Check Out, Check List, Login e Mensagens
		   dateHour.timeIn = null;
		   dateHour.timeOut = null;
		   dateHour.timeList = null;
		   dateHour.timeLogin = null;
		   dateHour.timeTermo = null;
		   dateHour.timeImage = null;
		   dateHour.timeMessage = null;
		   dateHour.timeMateriais = null;
		   dateHour.timeUniformes = null;
		   dateHour.timeDesbloqueio = null;
		  
		   //função para dia e hora atual do smartphone
			function createTime(){
			    var d = new Date();
			    var hour  = d.getHours(),
			    day = d.getDate(),
			    month = (d.getMonth() + 1),
			    minutes = d.getMinutes(),
			    seconds = d.getSeconds();

		        var fullDate = d.getFullYear() +"-"+ (month < 10 ? '0' + month:month) +"-"+ (day < 10 ? '0' + day:day);
		        
			    var fullTime = (hour < 10 ? '0'+hour:hour) +":"+ 
			    (minutes < 10 ? '0'+minutes:minutes) +":"+ 
			    (seconds < 10 ? '0'+seconds:seconds);

		        var all = fullDate +" "+ fullTime;
		        return all;
		    }

		   			//ações
		   	//função para data e hora de Check In
		   	dateHour.checkIn = function(){
    	    	dateHour.timeIn = createTime();
		    	return dateHour.timeIn;
		   	}

		   	//função para data e hora de Check Out
		  	dateHour.checkOut = function(){
		    	dateHour.timeOut = createTime();
		    	return dateHour.timeOut; 
		   	}

		   	//função para data e hora de Check Out
		  	dateHour.checkList = function(){
		    	dateHour.timeList = createTime();
		    	return dateHour.timeList; 
		   	}

		   	//função para data e hora de Login
		  	dateHour.checkLogin = function(){
		    	dateHour.timeLogin = createTime();
		    	return dateHour.timeLogin; 
		   	}
			
			//função para data e hora de Termo
		  	dateHour.checkTermo = function(){
		    	dateHour.timeTermo = createTime();
		    	return dateHour.timeTermo; 
		   	}

			//função para data e hora de email para Materiais
		  	dateHour.checkMateriais = function(){
		    	dateHour.timeMateriais = createTime();
		    	return dateHour.timeMateriais; 
		   	}

			//função para data e hora de email para Uniformes
		  	dateHour.checkUniformes = function(){
		    	dateHour.timeUniformes = createTime();
		    	return dateHour.timeUniformes; 
		   	}

			//função para data e hora da Imagem
		  	dateHour.checkImage = function(){
		    	dateHour.timeImage = createTime();
		    	return dateHour.timeImage; 
			   }
			   
			//função para data e hora da solicitação de desbloqueio
			dateHour.checkDesbloqueio = function(){
		    	dateHour.timeDesbloqueio = createTime();
		    	return dateHour.timeDesbloqueio; 
			}

		   	//função para data e hora da mensagem de Marketing
		  	dateHour.checkMessage = function(){
		  		var d = new Date();
		  		var day = d.getDate(),
		  		month = (d.getMonth() + 1);

		  		var fullDate = d.getFullYear() +"-"+ (month < 10 ? '0' + month:month) +"-"+ (day < 10 ? '0' + day:day);

		    	dateHour.timeMessage = fullDate;
		    	return dateHour.timeMessage; 
		   	}

		   	//função para limpar as variáveis
		  	dateHour.clearDate = function(){
		  		dateHour.timeIn = null;
		    	dateHour.timeOut = null;
		    	dateHour.timeList = null;
				dateHour.timeTermo = null;
				dateHour.timeImage = null;
				dateHour.timeMessage = null;
				dateHour.timeMateriais = null;
				dateHour.timeUniformes = null;
				dateHour.timeDesbloqueio = null;
		    }

		   	//retorno do objeto do service
	   		return dateHour;
	  }
})();// auto declaração
