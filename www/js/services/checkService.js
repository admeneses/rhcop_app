(function(){
	'use strict'; //indica erro, caso haja algo esteja desatualio ou com erro

	angular
		.module('starter')
		.factory('Check', Check);
 
		function Check($rootScope){
			var check = {};
	  	    
			//variaveis com as informações do funcionário
			check.id = null;
			check.nome = null;
			check.sobrenome = null;
			check.cargo = null;
			check.email = null;
			check.statusFunc = null;
			check.username = null;
			check.senha = null;
			check.idChecklist = null;
			check.lojaFunc = null;
			check.lojaLat = null;
			check.lojaLong = null;

			//variaveis para permissão
			check.gpsAtivo = null;
			check.mterosAtivo = null;
			check.cargoQuinhentos = null;
			
			//variaveis para exibição das lojas e horarios escaneados
			check.pontocol = null;
			check.pontocol2 = null;
			check.currentTimeIn = null;
			check.currentTimeOut = null; 

			//variaveis para exibição do modo de procedimento do CheckIn e/ou Checkout
			check.statusModeIn = null;
			check.statusModeOut = null;

			/* variável para o status de Check In, Check Out, Check List ou nenhum
				1 - Checkin/ Check List
				2 - Checkout
				3 - nenhum  
			*/
			check.verific = 3;
			check.verificList = 3; //Check List
			check.gps = 3;

	  	    // as funções serão usadas no mainCtrl
	  	    // função para determinar status igual a Check In
		    check.checkin = function () {
	  	    	check.verific = 1;
	        	$rootScope.$broadcast("verificacao", 1);
	     	}

	     	// função para determinar status igual a Check Out
	    	check.checkout = function () {
	    		check.verific = 2;
	    		$rootScope.$broadcast("verificacao", 2);
	    	}

	    	//função para informar se o Check List foi enviado 
	    	check.checklist = function () {
	    		check.verificList = 1;
	    		$rootScope.$broadcast("verificacaoList", 1);
			}
			
			check.gpsOn = function () {
				check.gps = 1;
				$rootScope.$broadcast("gps", 1);
			}

		   // função para determinar status igual a Check Out
			check.gpsOff = function () {
				check.gps = 2;
				$rootScope.$broadcast("gps", 2);
			}

			//função para limpar as informações do usuário ao realizar novo login
	    	check.clearUserInfo = function () {
	    		check.id = null;
				check.nome = null;
				check.sobrenome = null;
				check.cargo = null;
				check.email = null;
				check.statusFunc = null;
				check.username = null;
				check.senha = null;
				check.idChecklist = null;
				check.lojaFunc = null;
				check.lojaLat = null;
				check.lojaLong = null;
				check.gpsAtivo = null;
				check.metrosAtivo = null;
				check.cargoQuinhentos = null 
	    	}

			//função para limpar ultima entrada e saída da loja escaneada
	    	check.clearStore = function () {
	    		check.pontocol = null;
				check.pontocol2 = null;
				check.currentTimeIn = null;
				check.currentTimeOut = null;
				check.statusModeIn = null;
				check.statusModeOut = null; 
	    	}

	    	//função para deixar o ícone Contato preto, ou seja, sem check in e sem check out
	    	check.clearCheck = function () {
	    		check.verific = 3;
	    		$rootScope.$broadcast("verificacao", 3);
	    	}

	    	//função para deixar o item de Check List preto, ou seja, não foi efetuado
	    	check.clearCheckList = function () {
	    		check.verificList = 3;
	    		$rootScope.$broadcast("verificacaoList", 3);
	    	}

	    	return check;
	    }
})(); //autodeclaração