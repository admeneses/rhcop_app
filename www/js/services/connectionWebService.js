(function(){
	'use strict';

	angular
		.module('starter')
		.factory('WebService', WebService)
		WebService.$inject = ['$http', '$window'];

	function WebService($http, $window){
		//variaveis URL para envio dos webservices

		var url = 'http://www.wv-connect.com/RHCOP/webservices/';
		// var url = 'http://www.wv-connect.com/RHCOP/testWebservices/';
		// var url = 'http://192.168.1.120:80/Projetos_LEDi/RHCOP/webservices/';
		
		//função para receber os dados de Login do banco de dados do servidor (POST)
		var _doLogin = function(username, password, dataHora, status, versao, uuid, model, versionDevice) {
    		return $http({
			   "url": url + 'login_5727.php',
			   "method": "POST",
			   "data": {'username': username, 'password' : password, 'dataHora' : dataHora, 'status': status, 'versao' : versao, 'uuid' : uuid, 'model' : model, 'versionDevice' : versionDevice }
			})
		};

	    //função para enviar (fazer INSERT) os dados de Check In do banco de dados do servidor na tabela cartao_ponto
    	var _doCheckIn = function(funcionario, checkin, cartao_pontocol, latitude, longitude, status) {
    		return $http({
			   "url": url + "checkInOut_5727.php",
			   "method": "POST",
			   "data": {'funcionario': funcionario, 'checkin': checkin, 'checkout': '', 'cartao_pontocol': cartao_pontocol, 'latitude': latitude, 'longitude': longitude, 'status' : status}
			})
    	};
    	
    	//função para enviar (fazer INSERT) os dados de Check Out do banco de dados do servidor na tabela cartao_ponto
    	var _doCheckOut = function(funcionario, checkout, cartao_pontocol, latitude, longitude, status) {
        	return $http({
			   "url":url + "checkInOut_5727.php",
			   "method": "POST",
			   "data": {'funcionario': funcionario, 'checkin': '', 'checkout': checkout, 'cartao_pontocol': cartao_pontocol, 'latitude': latitude, 'longitude': longitude, 'status': status}
			})
    	};

    	//função para enviar (fazer INSERT) das questões de Check List no banco de dados do servidor na tabela check_list
    	var _doCheckList = function(historyAnswers, funcionario, loja, dataHora, questao6Text, questao11Text, status) {
        	return $http({
			   "url":url + "checkList_402.php",
			   "method": "POST",
			   "data": {'history': historyAnswers, 'funcionario' : funcionario, 'loja': loja, 'dataHora': dataHora, 'questao6Text' : questao6Text, 'questao11Text' : questao11Text, 'status' : status}
			})
    	};

    	//função para enviar email informando o horário dde check in ou check out
    	var _sendEmail = function(data) {
        	return $http({
			   "url":url + "sendMail_402.php",
			   "method": "POST",
			   "data": data
			})
    	};

    	//função para receber as mensagens de Marketing
    	var _marketing = function(dataHora) {
    		return $http({
			   "url": url + 'marketing_402.php',
			   "method": "POST",
			   "data": {'dataHora': dataHora}
			});
    	};

		//função para receber as rotas do funcionário
    	var _routes = function(funcionario, begin, end) {
    		return $http({
			   "url": url + 'routes_402.php',
			   "method": "POST",
			   "data": {'funcionario': funcionario, 'begin' : begin, 'end' : end}
			});
    	};
	    
		//função para receber as entradas e saídas do funcionário
    	var _report = function(value, begin, end, hasLoja) {
    		return $http({
			   "url": url + 'employee_402.php?value=' + value + '&begin=' + begin + '&end=' + end + '&hasLoja=' + hasLoja,
			   "method": "GET"
			});
    	};

		//função para enviar a solicitação de materiais por email
		var _emailMateriais = function(data) {
    		return $http({
			   "url": url + 'materiais_402.php',
			   "method": "POST",
			   "data": data
			});
    	};

		//função para enviar a solicitação de uniformes por email
		var _emailUniformes = function(data) {
    		return $http({
			   "url": url + 'uniformes_402.php',
			   "method": "POST",
			   "data": data
			});
    	};

		//função para receber questões do Check List
		var _receberCheckList = function(idChecklist) {
    		return $http({
			   "url": url + 'checklist_cadastro.php?action=GET&idChecklist=' + idChecklist,
			   "method": "GET"
			});
    	};

		// //função para enviar respostas do Check List
		var _enviarCheckList = function(checklist, funcionario, dataHora, loja) {
			return $http({
			   "url": (url + 'checklist_cadastro.php?action=POST&idChecklist=' + checklist.checklist.id),
			   "method": "POST",
			   "data": { 'checklist': checklist, 'funcionario' : funcionario, 'dataHora': dataHora, 'loja': loja }
			})
    	};

		// //função para enviar respostas do Check List
		var _enviarTermo = function(funcionario, dataHora, status, versao) {
			return $http({
			   "url": url + 'termo_510.php',
			   "method": "POST",
			   "data": { 'funcionario': funcionario,  'dataHora': dataHora, 'status': status, 'versao' : versao}
			})
		};
		
		//função para enviar a solicitação de transferencia por email
		var _emailTransferencia = function(data) {
			return $http({
				"url": url + 'transferencia.php',
				"method": "POST",
				"data": data
			});
		};

		//função para enviar email de atestado
		var _sendEmailAtestado = function(data) {
			return $http({
				"url":url + "sendMailAtestado.php",
				"method": "POST",
				"data": data
			})
		};

		// //função para desbloquear acesso de Login para colaboradores
		var _solicitarDesbloqueio = function(responsavel, username, dataHora) {
			return $http({
				"url": url + 'desbloqueio.php',
				"method": "POST",
				"data": { 'responsavel': responsavel, 'username' : username, 'dataHora': dataHora }
			})
		};
		
	    return {
	        doLogin: _doLogin,
	        // isLogged: _isLogged,
	       	doCheckIn: _doCheckIn,
	       	doCheckOut: _doCheckOut,
	       	doCheckList: _doCheckList,
	       	sendEmail: _sendEmail,
	       	marketing: _marketing,
			routes: _routes,
			report: _report,
			emailMateriais: _emailMateriais,
			emailUniformes: _emailUniformes,
			receberCheckList: _receberCheckList,
			enviarCheckList: _enviarCheckList,
			enviarTermo: _enviarTermo,
			emailTransferencia: _emailTransferencia,
			sendEmailAtestado: _sendEmailAtestado,
			solicitarDesbloqueio: _solicitarDesbloqueio
	    };
		
	}

})();