(function(){
	'use strict'; //indica erro, caso haja alguma coisa desatualiza ou um erro

	angular
		.module('starter')
		.factory('FileSystem', FileSystem);
		FileSystem.$inject = ['$cordovaFile'];
		
		function FileSystem($cordovaFile){
			var fileCtrl = {};
			fileCtrl.File = File;

			//classe
			function File(){
				var file = {};
				var dirEntry = null;

				document.addEventListener("deviceready", function () {
					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
					    // console.log('file system open: ' + fs.name);
					    dirEntry = fs.root;
					}, function (err) {
						console.log(err);
					});
				}, false);

				//Interfaces da classe File
				file.getFile   		 = getFile;
				file.readFile  		 = readFile;
				file.writeFile 		 = writeFile;
				// file.copy			 = copy;

				// selecionando arquivo
				function getFile(nameFile, callback) {
					dirEntry.getFile(nameFile, { create: true, exclusive: false }, function (fileEntry) {
						callback(fileEntry, null);
					
					}, function (err) {
						callback(null, err);
					})
				}

				// lendo arquivo
				function readFile(callback) {
					getFile("info.json", function (fileEntry, err) {
						if(err) return; //Caso de erro, ele não faz nada

						fileEntry.file(function (file) {
					        var reader = new FileReader();

					        reader.onloadend = function() {
					            // console.log("Successful file read: ");
					            callback(this.result, null);
					        };

					        reader.readAsText(file);

					    }, function (err) {
					    	console.log(null, err);
					    });
					})
				}

				// escrevendo no arquivo
				function writeFile(info, callback) {
					getFile("info.json", function (fileEntry, err) {
						if(err) return;

						// Create a FileWriter object for our FileEntry (log.txt).
					    fileEntry.createWriter(function (fileWriter) {

					        fileWriter.onwriteend = function() {
					            // console.log("Successful file write...");
					            readFile(function (data, err) {
					            	
					            	if(err) 
					            		callback(null, data); // Vejo se na leitura tivemos algum problema

					            	callback(data, null);
					            });
					        };

					        fileWriter.onerror = function (e) {
					            console.log("Failed file write: " + e.toString());
					            callback(null, e);
					        };

					        fileWriter.write(info);
					    });
					})
				}

				// COPY
				// function copy(){
				// 	//arquivo
				// 	$cordovaFile.copyFile(cordova.file., "info.json", cordova.file.externalDataDirectory, "copyInfo.json")
				// 	.then(function (success) {
				// 		console.log(success);
				// 	}, function (error) {
				// 		console.log(error);
				// 	});


				// 	// function createDirectory(rootDirEntry) {
				// 	// 	rootDirEntry.getDirectory('NewDirInRoot', { create: true }, function (dirEntry) {
				// 	// 		dirEntry.getDirectory('images', { create: true }, function (subDirEntry) {

				// 	// 			createFile(subDirEntry, "fileInNewSubDir.txt");

				// 	// 		}, onErrorGetDir);
				// 	// 	}, onErrorGetDir);
				// 	// }
				// }

				return file;
			}

			return fileCtrl;

	    }
})(); //autodeclaração