(function(){
	'use strict';

	angular
		.module('starter')
		.factory('Popup', Popup)
		Popup.$inject = ['$ionicPopup', '$state','Check', 'DateHour', '$window', 'Network', 'VERSION'];

		function Popup($ionicPopup, $state, Check, DateHour, $window, Network, VERSION){
			var popup = {};

			// alert para efetuar logout da aplicação pelo botão Sair do menu lateral
									//callback
			popup.logout = function(){
				$ionicPopup.alert({
					'title': 'Deseja realmente sair da aplicação?',
					'subTitle': '',
					'template': '',
					'buttons': [
						{
							'text': 'Não',
							'type': 'button-assertive',
							'onTap': function(e){
								//Do nothing
							}
						},
						{
							'text': 'Sim',
							'type': 'button-balanced',
							'onTap': function(e){
								$window.location.href = '#/login';
								//callback(); //passei uma função como parametro e estou chamando-a
							}
						}
					]
				});
			}

			// alert para sair e efetuar logout da aplicação com o botão voltar do android
			popup.sairApp = function(){
				$ionicPopup.alert({
					'title': 'Deseja realmente sair da aplicação?',
					'subTitle': '',
					'template': '',
					'buttons': [
						{
							'text': 'Não',
							'type': 'button-assertive',
							'onTap': function(e){
								//Do nothing
							}
						},
						{
							'text': 'Sim',
							'type': 'button-balanced',
							'onTap': function(e){
								navigator.app.exitApp();
							}
						}
					]
				});
			}

			// alert para efetuar o Check In antes de efetuar o Check Out
			// popup.efetuarCheckIn = function(){
			// 	$ionicPopup.alert({
			// 		'title': 'Efetuar Check-In!',
			// 		'subTitle': 'Por favor, efetue o Check-In antes de realizar o Check-Out.',
			// 		'template': '',
			// 		'buttons': [
			// 			{
			// 				'text': 'Ok',
			// 				'type': 'button-balanced',
			// 				'onTap': function(e){
			// 					//Do nothing
			// 				}
			// 			}
			// 		]
			// 	});
			// }

			// alert para avisar que o Check In foi efetuado com sucesso
			popup.checkinEfetuado = function(){
				$ionicPopup.alert({
					'title': 'Check-In realizado!',
					'subTitle': 'Você pode visualizar a data e hora do seu Check-In na tela de Presença (disponível no meu lateral).',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que o Check Out foi efetuado com sucesso
			popup.checkoutEfetuado = function(){
				$ionicPopup.alert({
					'title': 'Check-Out realizado!',
					'subTitle': 'Você pode visualizar a data e hora do seu Check-Out na tela de Presença (disponível no meu lateral).',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que o Check In não foi enviado para o banco de dados no servidor
			popup.enviarCheckInFail = function(){
				$ionicPopup.alert({
					'title': 'Não foi possível o envio do Check In!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-positive',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que o Check Out não foi enviado para o banco de dados no servidor
			popup.enviarCheckOutFail = function(){
				$ionicPopup.alert({
					'title': 'Não foi possível o envio do Check Out!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-positive',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que o GPS está desativado
			popup.conexaoGPS = function(){
				$ionicPopup.alert({
					'title': 'Não foi possível encontrar sua localização!',
					'subTitle': 'Ative seu GPS.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que o usuário ou senha estão incorretos para efetuar Login
			popup.usuarioInvalido = function(){
				$ionicPopup.alert({
					'title': 'Usuário e/ou senha inválido(s)!',
					'subTitle': 'Insira os dados corretamente para Logar na aplicação.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-positive',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que o Check List foi enviado para o banco de dados no servidor
			popup.enviarCheckList = function(){
				$ionicPopup.alert({
					'title': 'Check List enviado!',
					'subTitle': '',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-positive',
							'onTap': function(e){
								if(Check.cargo == "Supervisor Regional" || Check.cargo == "Supervisor Geral"){
									popup.takePictures();
								}else{
									//do nothing
								}
							}
						}
					]
				});
			}

			// alert para avisar que o Check List não foi enviado para o banco de dados no servidor
			popup.enviarCheckListFail = function(){
				$ionicPopup.alert({
					'title': 'Não foi possível o envio do Check List!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-positive',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert de "Bem vindo" ao logar na aplicação
			popup.welcome = function(){
				$ionicPopup.show({
					'title': 'Bem vindo '+ Check.nome + ' ' + Check.sobrenome +'!',
					'subTitle': Check.cargo,
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que o Login não foi realizado
			popup.loginFail = function(){
				$ionicPopup.alert({
					'title': 'Não foi possível realizar Login!',
					'subTitle': 'Tente novamente, problema de autenticação!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-positive',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que os Avisos de Marketing não estão disponíveis em Modo offline
			popup.marketing = function(){
				$ionicPopup.show({
					'title': 'Avisos indisponíveis!',
					'subTitle': 'Conecte seu dispositivo a internet para receber os avisos!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para avisar que as Rotas do funcionário não estão disponíveis em Modo offline
			popup.routes = function(){
				$ionicPopup.show({
					'title': 'Rotas indisponíveis!',
					'subTitle': 'Conecte seu dispositivo a internet para receber suas rotas!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// alert para o usuário lembrar de conectar seu dispostivo a rede para receber os Avisos de Marketing
			// ao logar na aplicação
			popup.avisos = function(){
				$ionicPopup.show({
					'title': Check.nome + ', lembre-se de conectar seu dispositivo a internet para receber os Avisos e suas Rotas!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			// funções para procedimento de Check List
			popup.pCheckList = function(){
				$ionicPopup.show({
					'title': Check.nome + ', deseja responder o Check List?',
					'buttons': [
						{
							'text': 'Não',
							'type': 'button-assertive',
							'onTap': function(e){
								//Do nothing
							}
						},
						{
							'text': 'Sim',
							'type': 'button-balanced',
							'onTap': function(e){
								$state.go('checkList');
							}
						}
					]
				});
			}

			popup.realizarCheckList = function(){
				$ionicPopup.show({
					'title': Check.nome + ', execute o procedimento de Saída para acessar o Check List!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			popup.checkListRespondido = function(){
				$ionicPopup.show({
					'title': Check.nome + ', você já respondeu o Check List!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			//função para alertar que não é possível visualizar seu rellatório sem conexão com internet
			popup.reportFail = function(){
				$ionicPopup.show({
					'title': Check.nome + ', você precisa ter conexão com internet para ver seu relatório!',
					'subTitle': 'Seus dados precisam estar na nuvem da RHCOP para serem exibidos no relatório.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			//função para tirar e/ou selecionar imagens
			popup.takePictures = function(){
				$ionicPopup.show({
					'title': 'Deseja selecionar ou tirar fotos da loja?',
					'buttons':[
						{
							'text': 'Não',
							'type': 'button-assertive',
							'onTap': function(e){
								$state.go('presenca');
							}
						},
						{
							'text': 'Sim',
							'type': 'button-balanced',
							'onTap': function(e){
								if(Network.status){
									$state.go('images');
								}else{
									popup.imagesFail();
								}
							}
						}
					]
				})
			}

			popup.imageCaptured = function(){
				$ionicPopup.show({
					'title': Check.nome + ', sua foto foi tirada/ selecionada com sucesso!',
					'subTitle': 'Sua foto está salva no seu dispositivo e você poderá enviá-la clicando em Enviar.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			popup.sendingImage = function(){
				$ionicPopup.show({
					'title': Check.nome + ', imagem enviada com sucesso!',
					'subTitle': 'Você pode tirar/ selecionar novas fotos e enviá-las!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			popup.sendingImageFail = function(){
				$ionicPopup.show({
					'title': Check.nome + ', não foi possível enviar sua fotografia!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			popup.imagesFail = function(){
				$ionicPopup.show({
					'title': Check.nome + ', não é possível enviar fotos em Modo Offline!',
					'subTitle': 'Ative sua internet e tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			//funções para solicitação de materiais
			// popup.materialLoja = function(){
			// 	$ionicPopup.show({
			// 		'title': Check.nome + ', é preciso escanear o QRCode da loja!',
			// 		'subTitle': 'Tente novamente!',
			// 		'template': '',
			// 		'buttons': [
			// 			{
			// 				'text': 'Ok',
			// 				'type': 'button-assertive',
			// 				'onTap': function(e){
			// 					//Do nothing
			// 				}
			// 			}
			// 		]
			// 	});
			// }

			popup.materialEnviado = function(){
				$ionicPopup.show({
					'title': Check.nome + ', sua solicitação foi enviada!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								$state.go('presenca');
							}
						}
					]
				});
			}

			popup.materialFail = function(){
				$ionicPopup.show({
					'title': Check.nome + ', sua solicitação não foi enviada!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			// popup.materialLojaEscaneada = function(){
			// 	$ionicPopup.show({
			// 		'title': Check.nome + ', loja escaneada!',
			// 		'subTitle': 'Agora descreva os materiais que deseja solicitar!',
			// 		'template': '',
			// 		'buttons': [
			// 			{
			// 				'text': 'Ok',
			// 				'type': 'button-balanced',
			// 				'onTap': function(e){
			// 					//do nothing
			// 				}
			// 			}
			// 		]
			// 	});
			// }

			popup.materialOffline = function(){
				$ionicPopup.show({
					'title': Check.nome + ', você precisa ter conexão com internet para solicitar os materiais!',
					'subTitle': 'Ative sua conexão e tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//funções para solicitação de uniformes
			popup.uniformesEnviado = function(){
				$ionicPopup.show({
					'title': Check.nome + ', sua solicitação foi enviada!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			popup.uniformesFail = function(){
				$ionicPopup.show({
					'title': Check.nome + ', sua solicitação não foi enviada!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			popup.uniformesOffline = function(){
				$ionicPopup.show({
					'title': Check.nome + ', você precisa ter conexão com internet para solicitar os Uniformes!',
					'subTitle': 'Ative sua conexão e tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//função para modo debug
			popup.modeDebugOn = function(){
				$ionicPopup.show({
					'title': 'Botão Debug ativado!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//função para funcionário inativo
			popup.inativoFunc = function(){
				$ionicPopup.show({
					'title': 'Funcionário Inativo!',
					'subTitle': 'Não autorizado logar no sistema.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//função para nenhum check list cadastrado
			popup.checklistNaoCadastrado = function(){
				$ionicPopup.show({
					'title': 'Você não possui nenhum checklist cadastrado!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								
							}
						}
					]
				});
			}

			//função para avisar sobre a conexão de internet para o Check List Dinamico
			popup.checklistOff = function(){
				$ionicPopup.show({
					'title': 'Você precisa ter conexão com internet para responder o Check List!',
					'subtitle': 'Conecte-se a internet!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								$state.go('presenca');
							}
						}
					]
				});
			}

			//função para envio dos Termos
			popup.termoOffline = function(){
				$ionicPopup.show({
					'title': 'Você precisa ter conexão com internet para concordar com os Termos!',
					'subtitle': 'Conecte-se a internet e tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			popup.termoFail = function(){
				$ionicPopup.show({
					'title': 'Não foi possível enviar sua aceitação de Termo!',
					'subtitle': 'Conecte-se a internet e tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//função para versao desatualizada
			popup.versao = function(){
				$ionicPopup.show({
					'title': 'Existe uma nova versão disponível!',
					'subTitle': 'Você está usando a versão: ' + VERSION.value + 
					'. Para atualizar procure pelo aplicativo na Google Play e clique em atualizar.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//funções para solicitação de transferência
			popup.transferenciaEnviado = function(){
				$ionicPopup.show({
					'title': Check.nome + ', sua solicitação foi realizada com sucesso!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								$state.go('presenca');
							}
						}
					]
				});
			}

			popup.transferenciaFail = function(){
				$ionicPopup.show({
					'title': Check.nome + ', não foi possível concluir a asolicitação!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			popup.transferenciaOffline = function(){
				$ionicPopup.show({
					'title': Check.nome + ', você precisa ter conexão com internet para solicitar Transferências!',
					'subTitle': 'Ative sua conexão e tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			popup.transferenciaInvalida = function(){
				$ionicPopup.show({
					'title': Check.nome + ', você precisa inserir uma data válida. A transferência pode ser realizada a partir de amanhã!',
					'subTitle': 'Tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//função para leitura válida de QRCode
			popup.qrcodeInvalido = function(){
				$ionicPopup.show({
					'title': Check.nome + ', escaneie um QRCode válido!',
					'subTitle': 'Tente novamente.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//função para raio de 500m
			popup.metrosInvalido = function(){
				$ionicPopup.show({
					'title': Check.nome + ', você não pode efetuar o procedimento fora da loja!',
					'subTitle': 'Tente novamente.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//funções para dispositivo não autorizado
			popup.dispositivoNaoAutorizado = function(){
				$ionicPopup.show({
					'title': 'Não é possível efetuar o Login através desse dispositivo!',
					'subTitle': 'Entre em contato com algum Supervisor para efetuar o desbloqueio.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			//funções para desbloqueio
			popup.desbloqueioOffline = function(){
				$ionicPopup.show({
					'title': Check.nome + ', não é possível utilizar a funcionalidade em Modo Offline!',
					'subTitle': 'Ative sua internet e tente novamente!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//Do nothing
							}
						}
					]
				});
			}

			popup.desbloqueioEfetuado = function(){
				$ionicPopup.show({
					'title': 'Desbloqueio efetuado com sucesso!',
					'subTitle': 'O colaborador pode efetuar o login em outro dispositivo.',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-balanced',
							'onTap': function(e){
								$state.go('presenca');
							}
						}
					]
				});
			}

			popup.desbloqueioNaoEfetuado = function(){
				$ionicPopup.show({
					'title': 'Falha no procedimento de desbloqueio!',
					'subTitle': 'Tente novamente. Verifique o nome de usuário informado!',
					'template': '',
					'buttons': [
						{
							'text': 'Ok',
							'type': 'button-assertive',
							'onTap': function(e){
								//do nothing
							}
						}
					]
				});
			}

			return popup;
		}	
})();