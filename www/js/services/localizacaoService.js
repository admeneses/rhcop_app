(function(){
	'use strict'; //indica erro, caso haja alguma coisa desatualiza ou um erro

	angular
		.module('starter')
		.factory('Localizacao', Localizacao);
		// Localizacao.$inject = ['Popup', 'Check', '$cordovaGeolocation','$rootScope'];
		
		function Localizacao(Popup, Check, $cordovaGeolocation, $rootScope, $timeout, $ionicPopup){
			var localizacao = {};
			localizacao.gps = 'OFF';

			// localizacao.GPS = function (){
			// 	console.log("Se passaram 1 minuto!");
	
			// 	document.addEventListener("deviceready", onDeviceReady, true);
				
			// 	function onDeviceReady() {
				
			// 		// gpsDetect = cordova.require('cordova/plugin/gpsDetectionPlugin');
				
			// 		// var checkButton = document.getElementById("check");
					
			// 		// checkButton.onclick = function() {
			// 			gpsDetect.checkGPS(onGPSSuccess, onGPSError);
			// 		// } 
					
			// 		function onGPSSuccess(on) {
			// 			if (on){
			// 				console.log("GPS is enabled");
			// 				localizacao.posicao();
			// 				localizacao.gps = 'ON';
			// 			} 
			// 			else{
			// 				console.log("GPS is disabled");
			// 				localizacao.gps = 'OFF';
							
			// 				$ionicPopup.show({
			// 					'title': 'GPS desativado. Deseja ativá-lo?',
			// 					'buttons':[
			// 						{
			// 							'text': 'Não',
			// 							'type': 'button-assertive',
			// 							'onTap': function(e){
			// 								//do nothing
			// 							}
			// 						},
			// 						{
			// 							'text': 'Sim',
			// 							'type': 'button-balanced',
			// 							'onTap': function(e){
			// 								gpsDetect.switchToLocationSettings(onSwitchToLocationSettingsSuccess, onSwitchToLocationSettingsError);
			// 							}
			// 						}
			// 					]
			// 				})
			// 			} 
			// 		}
					
			// 		function onGPSError(e) {
			// 			alert("Error : "+e);
			// 			localizacao.gps = 'OFF';
			// 		}
					
			// 		// var openSettingsButton = document.getElementById("openSettings");
			
			// 		// openSettingsButton.onclick = function() {
			// 		// }
			
			// 		function onSwitchToLocationSettingsSuccess() {
			// 			localizacao.posicao();
			// 			localizacao.gps = 'ON';
			// 		}
			
			// 		function onSwitchToLocationSettingsError(e) {
			// 			// alert("Error : "+e);
			// 			localizacao.gps = 'OFF';
			// 		}
			// 	}
			// }
			// initGPS();

			//variáveis para latitude e longitude de Check In e Check Out
			//Check in
			localizacao.lat = null;
			localizacao.longt = null;

			//Check Out
			// localizacao.lat2 = null;
			// localizacao.longt2 = null;		  	

			//função para encontrar Localização e posição do smarthpone
			localizacao.posicao = function(){
				document.addEventListener('deviceready', function () {
					// var watchID = navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 5000 });
					// function onSuccess(position) {
					//     localizacao.lat = position.coords.latitude;
					//     localizacao.longt = position.coords.longitude;
					//     localizacao.lat2 = position.coords.latitude;
					//     localizacao.longt2 = position.coords.longitude;
					// }

					// // onError Callback receives a PositionError object
					// // caso não encontre-as ou dê erro, elas são nulas
					// function onError(error) {
					// 	localizacao.lat = null;
					//     localizacao.longt = null;
					// 	localizacao.lat2 = null;
					//     localizacao.longt2 = null;
					// }

					$cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy : true}).then(function (position) {
						localizacao.lat = position.coords.latitude;
						localizacao.longt = position.coords.longitude;

						// alert(localizacao.lat, localizacao.longt);
						// localizacao.lat2 = position.coords.latitude;
						// localizacao.longt2 = position.coords.longitude;
					},function (error) {
						console.log(error.message);
						// localizacao.lat = null;
						// localizacao.longt = null;
						// localizacao.lat2 = null;
						// localizacao.longt2 = null;
					});
				})
			}
			return localizacao;
	    }
})(); //autodeclaração