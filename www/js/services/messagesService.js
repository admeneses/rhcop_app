(function(){
	'use strict'; //indica erro, caso haja algo esteja desatualio ou com erro

	angular
		.module('starter')
		.factory('Message', Message);
		Message.$inject = ['DateHour', 'DB', 'Network', 'WebService','Popup', 'Check'];
 
		function Message(DateHour, DB, Network, WebService, Popup, Check){
			var message = {};
			
			//variáveis para dia e horário das mensagens
			var currentTimeMensagem = DateHour.checkMessage();
			//nunhuma mensagem disponível
			message.noMessages = '';
			//array para salvar mensagens
			message.listMessages = [];
			//contador de mensagens
			message.contAvisos = 0;

			// buscando as mensagens no banco de dados do servidor
			message.request = function(){
				if(Network.status == true){
					WebService.marketing(currentTimeMensagem).success(function(data) {
						console.log(data);				   	
						if(data.result.geral == 'Sucesso') {
							var values = data.values;
							//buscando as mensagens
							//console.log(values.geral.length);
							for(var i = 0; i < values.geral.length; i++){
								//nao tem funcionários
								values.funcionarios[i] = values.funcionarios[i] || [];
								// busca mensagem para o ID do funcionário
								for(var j = 0; j < values.funcionarios[i].length; j++){
									if(Check.id == values.funcionarios[i][j]){
										var diff = true;
										
										for(var k = 0; k < message.listMessages.length; k++){
											//console.log(values.geral[i].mensagem, message.listMessages[k].mensagem);
											//console.log('entrou');
											// verificando se mensagem já existe
											if(message.listMessages[k].mensagem == values.geral[i].mensagem){
												diff = false;
												// console.log('entrou');
											}
										}

										//adicionando mensagem
										if(diff){
											message.listMessages.push({'mensagem' : values.geral[i].mensagem, 'link' : values.geral[i].link, 'data' : values.geral[i].created_at});
											message.contAvisos++;
											message.noMessages = '';
											
											//salvando no BD SQLite
											// DB.saveAvisos(values.geral[i].id, values.geral[i].mensagem, values.geral[i].link, values.geral[i].created_at).then(function(data){
											//    	console.log(data);
											// }, function(err){
											//    	console.log(err);
											// });
										}
									}
								}
							}

						}else {
							// console.log(data);
							message.listMessages = [];
							message.noMessages = 'Nenhum aviso disponível!';
						}	            
					});
				} else if(Network.status == false){
					/*DB.marketing().then(function(data){
						if(data.rows.length){
							console.log(data.rows);
							for(var i = 0; i < data.rows.length; i++){
								var values = data.rows.item(i);
								//console.log(values);
						    }
						    //avisos();
						}
					}, function(err){
						console.log(err);
					});*/
				} else{
					//nothing
				}
			}

			// message.getLocalMessages = function(){
			// 	DB.marketing().then(function(data){
			// 		if(data.rows.length){
			// 			console.log(data.rows);
			// 			for(var i = 0; i < data.rows.length; i++){
			// 				var values = data.rows.item(i);
			// 				//console.log(values);
			// 				message.listMessages.push({'mensagem' : values.mensagem, 'link' : values.link, 'data' : values.created_at});
			// 		    }
			// 		    //avisos();
			// 		}
			// 	}, function(err){
			// 		console.log(err);
			// 	});
			// }

			// limpando contador de mensagens visualizadas
			message.clearMessages = function(){
				message.contAvisos = 0;
				return message.contAvisos;
			}

	    	return message;
	    }
})(); //autodeclaração