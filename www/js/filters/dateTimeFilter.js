(function(){
	'use strict';

	angular
		.module('starter')
		.filter('dateTime', dateTime)

	//função para filtrar a exibição da data e hora
	function dateTime(){
		return function(input){
			var newInput = '';

			//verifica se está passando nulo
			if(input === ' ' || !input){
				newInput = input;
			}else{
				var year, month, day, hour, minutes;

				year = input.substring(0, 4);
				month = input.substring(5, 7);
				day = input.substring(8, 10);
				hour = input.substring(11, 13);
				minutes = input.substring(14, 16);

				newInput = day + "/" + month + "/" + year + " - " + hour + ":" + minutes;
			}

			return newInput;
		}
	}
})();