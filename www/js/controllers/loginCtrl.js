angular
	.module('starter')
	.controller('loginCtrl', loginCtrl)
	loginCtrl.$inject = ['$cordovaDevice', '$scope', '$state', 'Network', 'DB', 'Check', '$window', 'Localizacao', 'WebService','Popup', 'DateHour', 'User', 'Message', 'VERSION'];

	function loginCtrl($cordovaDevice, $scope, $state, Network, DB, Check, $window, Localizacao, WebService, Popup, DateHour, User, Message, VERSION){
		$scope.login = {};

		//lembrar nome de usuário
		var timeoutUser = null
		function initUsername() {
			timeoutUser = setTimeout(function(){
				DB.userSave().then(function(data){
					// console.log(data);
					if(data.rows.length){
						// for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(0);
							$scope.username = values.username;
							// alert(values.username);
						// }
					}else{
						$scope.username = '';
					}
				}).catch( function(err) {
					// console.log(err);
					clearTimeout(timeoutUser); // Limpa a requisição passada
					initUsername();
				})
			}, 2000)
		}

		initUsername();

		$scope.model = null, $scope.uuid = null, $scope.version = null;
		
		//informações do dispositivo
		document.addEventListener("deviceready", function () {
			// var device = $cordovaDevice.getDevice();

			// var cordova = $cordovaDevice.getCordova();

			var model = $cordovaDevice.getModel();

			var platform = $cordovaDevice.getPlatform();

			var uuid = $cordovaDevice.getUUID();

			var version = $cordovaDevice.getVersion();

			$scope.model 		 = model;
			$scope.uuid  		 = uuid;
			$scope.versionDevice = platform + ' - ' + version;
		}, false);


		//localStorage.clear();
		Check.clearUserInfo();
		Check.clearStore();
		Check.clearCheck();
		Check.clearCheckList();
		DateHour.clearDate();
		Message.clearMessages();

		// exibir div de conexão
		$scope.load = false;
		
		//exibindo versão na tela
		$scope.versao = VERSION.name + ' ' + VERSION.value;
		
		// função para efetuar Login
		$scope.sendLogin = function (username, password){
			var currentTimeLogin = DateHour.checkLogin();
			var modeLogin = '';
			var desatualizado = '';

			if(Network.status == true){
				Localizacao.posicao();
				//modeLogin = 'online';
				var status = 'Online';
				
				$scope.load = true;

				// chamando o service (connectionWebService) para fazer a autenticação do usuário
				WebService.doLogin(username, password, currentTimeLogin, status, VERSION.value, $scope.uuid, $scope.model, $scope.versionDevice).success(function(data) {
		            $scope.load = false;
		            console.log(data);
					
					desatualizado = data.result.tbVersao;

					if(!data.result.uuidValid){
						// return alert('Dispositivo não autorizado!');
						return Popup.dispositivoNaoAutorizado();
					}

					if(data.result.tbConfigMetros) {
						var values = data.values;  
						Check.metrosAtivo = values.tbConfigMetros.definido;
						console.log(Check.metrosAtivo);
					}

					if(data.result.tbConfigGPS){
						var values = data.values; 
						Check.gpsAtivo = values.tbConfigGPS.definido;
						console.log(Check.gpsAtivo);
					}

					if(data.result.tbfuncionario) {
						var values = data.values;         	
						// grava os dados no Check Service
						Check.statusFunc = values.tbfuncionario.status;
						Check.id = values.tbfuncionario.id;
						Check.nome = values.tbfuncionario.nome;
						Check.sobrenome = values.tbfuncionario.sobrenome;
						Check.cargo = values.tbfuncionario.cargo;
						Check.email = values.tbfuncionario.email;
						Check.idChecklist = values.tbfuncionario.idChecklistLayout;
						Check.lojaFunc = values.tbfuncionario.lojaCodigo + " " + values.tbfuncionario.lojaFunc;
						Check.lojaLat = values.tbfuncionario.lojaLat;
						Check.lojaLong = values.tbfuncionario.lojaLong;
						Check.cargoQuinhentos = values.tbfuncionario.cargoQuinhentos;
						// Check.cargoQuinhentos = 1;
		
						Check.username = username;
						Check.senha = password;
						
						console.log(Check.lojaFunc, Check.lojaLat, Check.lojaLong);
						console.log(Check.cargoQuinhentos, values.tbfuncionario.cargoQuinhentos);

						//salvando no BD SQLite
						DB.checkUser(values.tbfuncionario.id, values.tbfuncionario.nome, values.tbfuncionario.sobrenome, values.tbfuncionario.email, username, password, values.tbfuncionario.cargo, values.tbfuncionario.status, values.tbfuncionario.idChecklistLayout).then(function(data){
							// console.log(data);
							DB.updateUser(values.tbfuncionario.id, values.tbfuncionario.nome, values.tbfuncionario.sobrenome, values.tbfuncionario.email, username, password, values.tbfuncionario.cargo, values.tbfuncionario.status, values.tbfuncionario.idChecklistLayout).then(function(data){
								// console.log(data);
							}, function(err){
								console.log(err);
							});
						}, function(err){
							console.log(err);
						});

						//salvando no BD SQLite
						DB.checkConfiguracao(1, Check.lojaFunc, Check.lojaLat, Check.lojaLong, Check.gpsAtivo, Check.metrosAtivo, Check.cargoQuinhentos).then(function(data){
							// console.log('Configuração salva');
						}, function(err){
							console.log(err);
						});

						if(Check.statusFunc == 'INA'){
							Popup.inativoFunc();
						}else{
							// DB.selectTermo().then(function(data){
							// 	if(data.rows.length){
							// 		for(var i = 0; i < data.rows.length; i++){
							// 			var values = data.rows.item(i);
							// 			if(Check.id == values.id && values.status == 1){
							// 				// redireciona para o sistema
							// 				$window.location.href = '#/presenca';
							// 				Popup.welcome();
							// 			}else{
							// 				$window.location.href = '#/termo';
							// 			}
							// 		}
							// 	}else{
							// 		$window.location.href = '#/termo';
							// 	}
							// })
							DB.selectTermoFunc(Check.id).then(function(data){
								if(data.rows.length){
									// redireciona para o sistema
									$window.location.href = '#/presenca';
									Popup.welcome();
									if(desatualizado == 'Desatualizado'){
										// $window.location.href = '#/versao';
										Popup.versao();
									}
								}else{
									$window.location.href = '#/termo';
								}
							})
						}
					}else if(!data.result.tbfuncionario){
						// console.log(data.result.tbfuncionario);
						// exibe um alert informando que o username e/ou senha estão inválidos
						Popup.usuarioInvalido();
					}else{
						var mensagem = 'Erro ao salvar Login Online';
						var dataHora = currentTimeLogin;
						var erro = data.result.tbfuncionario;
						console.log(mensagem, erro, dataHora);

						DB.saveDebug(mensagem, erro, dataHora).then(function(data){
							console.log('Debug Login Online salvo!');
						}, function(err){
							console.log('Erro ao salvar Login Online!', err);
						});
					} 
						
		        });
			}else if(Network.status == false){
				//modeLogin = 'offline';
				var status = 'Offline';
				$scope.load = true;
				//alert('Você não possui conexão com internet!');
				DB.login().then(function(data){
					if(data.rows.length){
						//console.log(data.rows);
						var logado = 0;
						for(var i = 0; i < data.rows.length; i++){
							var values = data.rows.item(i);
							//console.log(values);
							if(username === values.username && password === values.senha){
								Check.id = values.id;
				                Check.nome = values.nome;
				                Check.sobrenome = values.sobrenome;
				                Check.cargo = values.cargo;
				                Check.email = values.email;
								Check.statusFunc = values.status;
								Check.idChecklist = values.idChecklist;

								if(values.status == 'INA'){
									logado = 2;
								}else{
									DB.selectConfiguracao().then(function(data){
										if(data.rows.length){
											//console.log(data.rows);
											for(var i = 0; i < data.rows.length; i++){
												var values = data.rows.item(i);
												Check.lojaFunc = values.lojaFunc;
												Check.lojaLat = values.lojaLat;
												Check.lojaLong = values.lojaLong;
												Check.gpsAtivo = values.gpsAtivo;
												Check.metrosAtivo = values.metrosAtivo;
												Check.cargoQuinhentos = values.cargoQuinhentos;
												
												console.log(Check.lojaFunc, Check.lojaLat, Check.lojaLong);
												console.log(Check.cargoQuinhentos, values.cargoQuinhentos);
											}
										}else{
											//do nothing
										}
									}, function(err){
										console.log(err);
									});

									//salvando os dados de Login Offline no SQLite
									var usernameOffline = values.username;
									var passwordOffline = values.senha;
									var versao = VERSION.value;

									// console.log(usernameOffline, currentTimeLogin, status, versao);

									DB.saveLogin(usernameOffline, passwordOffline, currentTimeLogin, status, versao, $scope.uuid, $scope.model, $scope.versionDevice).then(function(data){
										//desativando div "enviando"
										$scope.load = false;
										DB.selectTermoFunc(Check.id).then(function(data){
											if(data.rows.length){
												// redireciona para o sistema
												$window.location.href = '#/presenca';
												Popup.avisos();
												Popup.welcome();
											}else{
												$window.location.href = '#/termo';
											}
										})
										// console.log('Login Salvo');
										// $window.location.href = '#/presenca';
										// Popup.avisos();
										// Popup.welcome();
									}, function(err){
										//desativando div "enviando"
										$scope.load = false;
										console.log(err);
										Popup.loginFail();

										var mensagem = 'Erro ao salvar Login Offline';
										var dataHora = currentTimeLogin;
										console.log(mensagem, err, dataHora);

										DB.saveDebug(mensagem, err, dataHora).then(function(data){
											console.log('Debug Login Offline salvo!');
										}, function(err){
											console.log('Erro ao salvar Debug Login Offline!', err);
										});
									});

									//salvando os dados no arquivo.json
									// var info = {
									//  "username": values.username,
									// 	"password": values.senha,
									// 	"currentTimeLogin": currentTimeLogin,
									// 	"status" : status,
									// 	"versao" : VERSION.value
									// }

									// User.save(info, function (data, err) {
									//     $scope.load = false;
									// 	// console.log(data, err);
									// 	if(err){
									// 		console.log(err);
									// 		Popup.loginFail();
									// 	}else{
									// 		$window.location.href = '#/presenca';
									// 		Popup.avisos();
									// 		Popup.welcome();
									// 	}	
									// })

									logado = 1;
									break;
								}
							}
						}
						
						if(logado == 0){
							$scope.load = false;
							Popup.usuarioInvalido();
						}else if(logado == 2){
							$scope.load = false;
							Popup.inativoFunc();
						}
					}else{
						$scope.load = false;
						alert('Nenhum usuário cadastrado no banco! (Modo offline)');
					}
				}, function(err){
					console.log(err);
				});
			}else{
				//  Popup.usuarioInvalido();
			}
			//$window.location.href = '#/presenca';
		}
	}
