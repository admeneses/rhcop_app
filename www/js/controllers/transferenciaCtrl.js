angular
.module('starter')
.controller('transferenciasCtrl', transferenciasCtrl)
transferenciasCtrl.$inject = ['$scope', 'Network', 'Popup', 'DateHour', 'WebService', 'Check'];

function transferenciasCtrl($scope, Network, Popup, DateHour, WebService, Check){
    $scope.send = false;

    $scope.solicitar = function(colaborador, lojaAtual, lojaFinal, dataTransferencia){
        var d = new Date();
        day = d.getDate(),
        month = (d.getMonth() + 1);

        var fullDate = d.getFullYear() +"-"+ (month < 10 ? '0' + month:month) +"-"+ (day < 10 ? '0' + day:day);

        var newDateTransf = dataTransferencia.getFullYear() + '-' + (dataTransferencia.getMonth() + 1) + '-' + dataTransferencia.getDate();
        
        if(moment(newDateTransf).isAfter(fullDate)){
            if(Network.status){
                $scope.send = true;
                var currentTime = DateHour.checkMateriais();
                var data = {"funcionario": Check.id, "nome": Check.nome, "sobrenome": Check.sobrenome, "cargo": Check.cargo, "colaborador" : colaborador, "lojaAtual": lojaAtual, "lojaFinal": lojaFinal, "dataHora" : currentTime, "dataTransferencia" : newDateTransf};
                WebService.emailTransferencia(data).success(function(data) {
                    $scope.send = false;
                    // console.log(data);
                    if(data.result == 'Sucesso'	) {
                        Popup.transferenciaEnviado();
                    }else{
                        console.log(data);
                        Popup.transferenciaFail();
                    }
                });
            }else{
                Popup.trnsferenciaOffline();
            }
        }else{
            Popup.transferenciaInvalida();
        }
    }
}