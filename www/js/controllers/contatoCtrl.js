angular
	.module('starter')
	.controller('contatoCtrl', contatoCtrl)
	contatoCtrl.$inject = ['$scope', '$stateParams', '$state', 'VERSION', 'User'];

	function contatoCtrl($scope, $stateParams, $state, VERSION, User){
		//exibindo versão na tela
		$scope.versao = VERSION.name + ' - ' + VERSION.value; 
		$scope.data = VERSION.date;

		// função para verificar para qual tela o botão "voltar" deve realizar
		// tela de Login ou Controle de Presença
		$scope.redirectBack = function(){
			$state.go($stateParams.previousState);
		}

		//verificar o parametro no contato.html
		$scope.next = $stateParams.previousState;

		$scope.dados = function(){
			User.dados();
		}

		$scope.dadosSQLite = function(){
			User.dadosSQLite();
		}
	}