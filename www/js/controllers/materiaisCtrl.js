angular
	.module('starter')
	.controller('materiaisCtrl', materiaisCtrl)
	materiaisCtrl.$inject = ['$scope', 'Network', 'Popup', 'DateHour', 'WebService', 'Check'];

	function materiaisCtrl($scope, Network, Popup, DateHour, WebService, Check){
		// $scope.fail = true;
		// $scope.semLoja = 'Escaneie o QRCode da loja';
		$scope.send = false;

		// $scope.escanear = function(){
		// 	document.addEventListener('deviceready', function () {
		// 		cordova.plugins.barcodeScanner.scan(function (result) {
		// 			if(result.text){
		// 				$scope.fail = false;
		// 				$scope.loja = result.text;
		// 				Popup.materialLojaEscaneada();
		// 			}
		// 		},function (error) {
		// 			//alert("O escaneamento falhou, tente novamente! " + error);
		// 		},

		// 		// configurações da tela do QRCode Scanner
		// 		{
		// 			"preferFrontCamera" : false, // iOS and Android
		// 			"showFlipCameraButton" : false, // iOS and Android
		// 			"prompt" : "Escaneie o QR Code da loja", // supported on Android only
		// 			"formats" : "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
		// 			"orientation" : "portrait" // Android only (portrait|landscape), default unset so it rotates with the device
		// 		});
		// 	})
		// }

		$scope.solicitar = function(loja, materiais){
			if(Network.status){
				$scope.send = true;
				// if($scope.fail){
				// 	$scope.send = false;
				// 	Popup.materialLoja();
				// }else{
					var currentTime = DateHour.checkMateriais();
					var data = {"funcionario": Check.id, "nome": Check.nome, "sobrenome": Check.sobrenome, "cargo": Check.cargo, "loja": loja, "materiais": materiais, "dataHora" : currentTime};
					WebService.emailMateriais(data).success(function(data) {
						$scope.send = false;
						// console.log(data);
						if(data.result == 'Sucesso'	) {
							Popup.materialEnviado();
						}else{
							console.log(data);
							Popup.materialFail();
						}
			    	});
				// }
			}else{
				Popup.materialOffline();
			}
		}
	}