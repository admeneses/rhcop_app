angular
	.module('starter')
	.controller('uniformesCtrl', uniformesCtrl)
	uniformesCtrl.$inject = ['$scope', 'Network', 'Popup', 'DateHour', 'WebService', 'Check'];

	function uniformesCtrl($scope, Network, Popup, DateHour, WebService, Check){

		$scope.send = false;
		$scope.loja = '';
		$scope.uniformeFunc = '';
		
		$scope.uniformes = {
			camiseta: {tamanho: 'P', qtd: "1"},
			moletom:  {tamanho: 'P', qtd: "1"},
			bota: {tamanho: 38, qtd: "1"},
			bone: {qtd: "1"},
			avental: {qtd: "1"}
		};

		$scope.solicitar = function(loja, uniformeFunc, uniformes, observacao){
			if(Network.status){
				$scope.send = true;
                var currentTime = DateHour.checkUniformes();
                var data = {"funcionario": Check.id, "nome": Check.nome, "sobrenome": Check.sobrenome, "cargo": Check.cargo, "dataHora" : currentTime, "loja": loja, "uniformeFunc" : uniformeFunc, "uniformes" : uniformes, "observacao" : observacao};
                WebService.emailUniformes(data).success(function(data) {
                    $scope.send = false;
                    // console.log(data);
                    if(data.result == 'Sucesso'	) {
						$scope.uniformes = {
							camiseta: {tamanho: 'P', qtd: "1"},
							moletom:  {tamanho: 'P', qtd: "1"}, 
							bota: {tamanho: 38, qtd: "1"},
							bone: {qtd: "1"},
							avental: {qtd: "1"}
						};

						$scope.loja = '';
						$scope.uniformeFunc = '';
                        Popup.uniformesEnviado();
                    }else{
						console.log(data);
                        Popup.uniformesFail();
                    }
                });
			}else{
				Popup.uniformesOffline();
			}
		}
	}