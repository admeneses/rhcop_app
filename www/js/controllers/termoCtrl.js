angular
	.module('starter')
	.controller('termoCtrl', termoCtrl)
	termoCtrl.$inject = ['$scope', '$stateParams', '$state', 'User', 'DB', 'Popup', 'WebService', 'Check', '$window', 'DateHour', 'Network', 'VERSION'];

	function termoCtrl($scope, $stateParams, $state, User, DB, Popup, WebService, Check, $window, DateHour, Network, VERSION){
		$scope.send = false;
		
		//função para confirmar e enviar ciencia dos termos
		$scope.confirmacao = function(){
			var currentTimeTermo = DateHour.checkTermo();
			var funcionario = Check.id;
			var versao = VERSION.value;
			var status = '';
			var statusTermo = 1;

			if(Network.status == true){
				$scope.send = true;
				status = 'Online';

				WebService.enviarTermo(funcionario, currentTimeTermo, status, versao).success(function(data) {
					console.log(data);
					// desativando div de "enviando"
					$scope.send = false;
					if(data.result == 'Sucesso') {
						DB.saveTermo(Check.id, statusTermo).then(function(data){
							$window.location.href = '#/presenca';
							Popup.welcome();
						}, function(err){
							console.log(err);
							Popup.termoFail();
						});
					}else {
						Popup.termoFail();
					}	            
				});
			}else{
				status = 'Offline';
				DB.saveTermo(Check.id, statusTermo).then(function(data){
					DB.saveTermoOffline(funcionario, currentTimeTermo, status, versao).then(function(data){
						//desativando div "enviando"
						$scope.load = false;
						// redireciona para o sistema
						$window.location.href = '#/presenca';
						Popup.avisos();
						Popup.welcome();
					}, function(err){
						//desativando div "enviando"
						$scope.load = false;
						console.log(err);
						Popup.termoFail();

						var mensagem = 'Erro ao salvar Termo Offline';
						var dataHora = currentTimeTermo;
						// console.log(mensagem, err, dataHora);

						DB.saveDebug(mensagem, err, dataHora).then(function(data){
							console.log('Debug Termo Offline salvo!');
						}, function(err){
							console.log('Erro ao salvar Debug Termo Offline!', err);
						});
					});
				}, function(err){
					console.log(err);
					Popup.termoFail();
				});
			}
		}
	}