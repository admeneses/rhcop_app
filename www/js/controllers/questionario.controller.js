(function() {
    'use strict';

    angular
        .module('starter')
        .controller('QuestionarioCtrl', questionarioCtrl);
        questionarioCtrl.$inject = ['$q', '$scope', '$state', 'ChecklistService', '$window', 'Popup', 'Check', 'Network', 'DateHour', 'DB'];

        function questionarioCtrl($q, $scope, $state, ChecklistService, $window, Popup, Check, Network, DateHour, DB) {
            var vm = this;
            var init = true;

            // exibir div de carregamento
		    $scope.load = false;

            // exibir div de envio
		    $scope.send = false;

            vm.checklist = null;

            // vm.checklist = {
            //     info: {
            //         id: 1,
            //         title: 'Primeiro',
            //         subtitle: '',
            //         version: 1
            //     },
            //     questions: [
            //         { id: 1, title: 'Primeira questão', type: 0, options: [], answer: 'Resposta do bom' },
            //         { id: 2, title: 'Segunda questão', type: 1, options: [], answer: 'Resposta do mal' },
            //         { id: 3, title: 'Terceira questão', type: 2, options: [{title: 'opção 01'}, {title: 'opção 02'}], answer: 'Opção 01' },
            //         // { id: 4, title: 'Quarta questão', type: 3, options: [{title: 'opção 01'}, {title: 'opção 02'}] },
            //         { id: 5, title: 'Quinta questão', type: 4, options: [{title: 'opção 01'}, {title: 'opção 02'}], answer: 'Opção 02' }
            //     ]
            // } // inicializa-lo NULL

            // vm.count            = 3;
            // vm.size             = vm.checklist.questions.length; // Mudar para 0 depois
            // vm.question_default = vm.checklist.questions[0]; // Obtem a primeira questão a ser renderizada (mudar para NULL depois)
            // vm.question_types   = ChecklistService.types; // Armazena os tipos de questões disponíveis

            vm.count            = 0;
            vm.size             = 0; // Mudar para 0 depois
            vm.question_default = null; // Obtem a primeira questão a ser renderizada (mudar para NULL depois)
            vm.question_types   = ChecklistService.types; // Armazena os tipos de questões disponíveis

             if(init){
                // Inicializa o checklist
                loadChecklist();
                init = false;
            }

            function loadChecklist() {
                if(Network.status == true){
                    $scope.load = true;
                    var response = ChecklistService.getChecklist();
                    response
                        .then( function(res) {
                            $scope.load = false;
                            if(!res.hasChecklist) {
                                $state.go('presenca');
                                Popup.checklistNaoCadastrado();
                            }else{
                                initChecklist(res.checklist);
                            }
                        })
                        .catch( function(err) {
                            console.log(err);
                        })
                }else{
                    // console.log('Sem internet');
                    // Popup.checklistOff();
                    DB.selectCheckListDinamico().then(function(data){
                        // console.log(data.rows.length);
                        // console.log(data);
                        if(data.rows.length){
                            var naoCadastrado = false;
                            for(var i = 0; i < data.rows.length; i++){
                                var value = data.rows.item(i);
                                // console.log(value.idChecklist,ar Check.idChecklist);
                                // console.log(value.checklist);

                                // console.log(value.idChecklist, Check.idChecklist);
                                var valid = parseInt(Check.idChecklist);
                                console.log(value.idChecklist, valid);
                                if(value.idChecklist == valid && value.funcionario == Check.id){
                                    $scope.load = false;
                                    // var response = value.checkList;
                                    value.checklist = JSON.parse(value.checklist);

                                    // console.log(response);

                                    var response = getCheckListOff();

                                    function getCheckListOff(){
                                        var response = { hasChecklist: false, checklist: null };

                                        return $q( function(resolve, reject) {
                                            if(!Check.idChecklist || Check.idChecklist == 0)
                                                return resolve(response);
    
                                            // console.log(checklist.json);
                                            if(value.checklist != 'null') {
                                                response.hasChecklist = true;
                                                response.checklist    = {
                                                    info: value.checklist.checklist,
                                                    questions: value.checklist.questions
                                                }
                                            }
                                            resolve(response);
                                        })
                                    }
            
                                    // var response = { hasChecklist: false, checklist: null };
                                    // if(value.checklist != 'null') {
                                    //     response.hasChecklist = true;
                                    //     response.checklist    = {
                                    //         info: value.checklist.checklist,
                                    //         questions: value.checklist.questions
                                    //     }
                                    // }

                                    response.then( function(res) {
                                        $scope.load = false;
                                        if(!res.hasChecklist) {
                                            $state.go('presenca');
                                            // Popup.checklistNaoCadastrado();
                                        }else{
                                            initChecklist(res.checklist);
                                        }
                                    })
                                    .catch( function(err) {
                                        console.log(err);
                                    })

                                    naoCadastrado = true;
                                    break;
                                    // var response = value.checklist;
                                    // response
                                    //     .then( function(res) {
                                    //         $scope.load = false;
                                    //         if(!res.hasChecklist) {
                                    //             $state.go('cronograma');
                                    //             Popup.checklistNaoCadastrado();
                                    //         }else{
                                    //             initChecklist(res.checklist);
                                    //         }
                                    //     })
                                    //     .catch( function(err) {
                                    //         console.log(err);
                                    //     })
                                }else{
                                    // $scope.naoCadastrado = false;
                                    // Popup.checklistNaoCadastrado();
                                }
                            }

                            if(naoCadastrado == false){
                                $state.go('presenca');
                                Popup.checklistNaoCadastrado();
                            }
                        }else{
                            //do nothing
                            // Popup.checklistNaoCadastrado();
                        }
                    }, function(err){
                        console.log(err);
                    })
                }
            }

            function initChecklist(checklist) {
                checklist.questions.map(function(question) {
                    question.options = JSON.parse(question.options);
                });

                vm.checklist        = checklist;
                vm.size             = vm.checklist.questions.length;
                vm.question_default = vm.checklist.questions[0];
            }

            // Actions

            vm.next         = next;
            vm.previous     = previous;
            vm.endChecklist = endChecklist;
            vm.updateAnswer = updateAnswer;

            function updateAnswer(){
                var options = vm.question_default.options.filter(function(option){
                    if(option.checked)
                        return true;
                    return false;
                })    

                vm.question_default.answer = options.map(function(option){
                    return option.title;
                });

                vm.question_default.answer = vm.question_default.answer.join(',');
            }

            function next() {
                vm.count++;
                changeQuestion();
            }

            function previous() {
                vm.count--;
                changeQuestion();
            }

            function changeQuestion() {
                vm.question_default = vm.checklist.questions[vm.count];
            }

            function endChecklist() {
                if(Network.status){
                    $scope.send = true;
                    var send_checklist = {
                        checklist: vm.checklist.info,
                        questions: vm.checklist.questions
                    };

                    send_checklist.questions = send_checklist.questions.map(function(question){
                        if(typeof question.answer == "object"){
                            question.answer = question.answer.title;
                        }
                        return question;
                    });
                    
                    var currentTimeList = DateHour.checkList();

                    ChecklistService.sendChecklist(send_checklist, Check.id, currentTimeList, Check.pontocol2)
                        .then( function(res) {
                            $scope.send = false;
                            console.log(res);
                            // exibindo PopUp de check list enviado
                            Popup.enviarCheckList();
                            // atualizando cor de check list enviado
                            Check.checklist();
                            // redireciona para a tela principal do app
                            $state.go('presenca');
                        })
                        .catch( function(err) {
                            $scope.send = false;
                            console.log(err);
                            // exibe um PopUp sobre o envio do check list ter falhado
                            Popup.enviarCheckListFail();
                            $scope.verificList = 2;
                        })
                }else{
                    var currentTimeList = DateHour.checkList();

                    var checklist = JSON.stringify(vm.checklist);
                    // console.log(checklist);
                    //salvando no BD SQLite
                    DB.saveCheckListDinamicoResposta(Check.id, checklist, currentTimeList, Check.pontocol2).then(function(data){
                        // exibindo PopUp de checki list enviado
                        Popup.enviarCheckList();   
                        // atualizando cor de check list enviado
                        Check.checklist();
                        // redireciona para a tela principal do app
                        $state.go('presenca');
                    }, function(err){
                        // exibe um PopUp sobre o envio do check list ter falhado
                        Popup.enviarCheckListFail();
                        
                        var mensagem = 'Erro ao salvar CheckList Dinamico Offline';
                        var dataHora = $scope.currentIn;
                        console.log(mensagem, err, dataHora);

                        DB.saveDebug(mensagem, err, dataHora).then(function(data){
                            console.log('Debug CheckList Dinamico Offline salvo!');
                        }, function(err){
                            console.log('Erro ao salvar CheckList Dinamico Offline!', err);
                        });
                    });
                }
            }
        }
})();