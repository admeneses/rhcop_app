(function(){
angular
	.module('starter')
	.controller('checkListCtrl', checkListCtrl)
	checkListCtrl.$inject = ['$scope', '$state','Popup', 'QUESTIONS', 'WebService', 'FileSystem', 'Network', 'User', 'DateHour', 'Check', '$ionicPopup'];

	function checkListCtrl($scope, $state, Popup, QUESTIONS, WebService, FileSystem, Network, User, DateHour, Check, $ionicPopup){
		var vm = this;
		//console.log(checkList);

		$scope.send = false;

	    vm.historyAnswers = [];
	    vm.answer = '';
	    vm.count = 0;
	    vm.size = QUESTIONS.length - 1;
	    vm.question = null;
	    vm.title = '';
	    vm.subtitle = '';
	    vm.questao6Text = null;
		vm.questao11Text = null;

	    //Essential functions
	    function addInHistory(question, pos){
	      if(vm.historyAnswers[pos])
	        vm.historyAnswers[pos].answer = vm.answer;
	      else
	        vm.historyAnswers.push({'question': question.title, 'subtitle' : question.subtitle ,'answer': vm.answer});
	    }

		// altera questão
	    function changeQuestion(){
	        vm.question = QUESTIONS[vm.count];
	    }

		// altera título da questão
	    function changeTitle(){
	        vm.title = "Questão " + (vm.count + 1);
	    }

	    initQuestion();
	    function initQuestion(){
	    	changeQuestion();
	        changeTitle();
	    }

	    //Normal functions
	    vm.endQuestions = endQuestions;
	    vm.next = next;
	    vm.previous = previous;

		// fim das questões e envio do questionário para o servidor
	    function endQuestions(question){
	       addInHistory(question)
	       
		    for(var i = 0; i < vm.historyAnswers.length; i++) {
				if(vm.historyAnswers[i].answer.obs){
				  vm.questao6Text = vm.historyAnswers[i].answer.obs;
				}

				if(vm.historyAnswers[i].answer.obs2)
				  vm.questao11Text = vm.historyAnswers[i].answer.obs2;
				}

				// hora e data do check list
				var currentTimeList = DateHour.checkList();

		    //    console.log(vm.historyAnswers);
		       if(Network.status == true){
				   //definindo conexão com internet como Online
				   var status = 'Online';
				   // desativando div de "enviando"
				   $scope.send = true;

				   // enviando informações para a tabela de check list no banco de dados do servidor
			        WebService.doCheckList(vm.historyAnswers, Check.id,  Check.pontocol2, currentTimeList, vm.questao6Text, vm.questao11Text, status).success(function(data) {
			        	//console.log(data);
						// desativando div de "enviando"
						$scope.send = false;
						if(data.result == 'Sucesso'	) {
							// exibindo PopUp de checki list enviado
							Popup.enviarCheckList();
							// atualizando cor de check list enviado
							Check.checklist();
							// redireciona para a tela principal do app
							$state.go('presenca');
						}else {
							// exibe que não foi possível enviar os dados para o banco do servidor 
							//console.log(data.result);
							// exibe um PopUp sobre o envio do check list ter falhado
							Popup.enviarCheckListFail();
							$scope.verificList = 2;
							$state.go('presenca');
						}	            
					});
		       }else if(Network.status == false){
				   	// ativando div de "enviando"
					$scope.send = true;
					//definindo conexão com internet como Offline
					var status = 'Offline';

					// armazenando informações para serem salvas no arquivo.json
		       		var info = {
					 	 "questoes": vm.historyAnswers,
						 "funcionario": Check.id,
						 "loja":  Check.pontocol2,
						 "checklist": currentTimeList,
						 "questao6Text" : vm.questao6Text,
						 "questao11Text" : vm.questao11Text,
						 "status" : status
					}

					// salvando informações no arquivo.json
					User.save(info, function (data, err) {
					    // desativando div de "enviando"
						$scope.send = false;
						// console.log(data, err);
						
						if(err){
							console.log(err);
							Popup.enviarCheckListFail();
						}else{				
							// exibindo PopUp de checki list enviado
							Popup.enviarCheckList();
							// redireciona para a tela principal do app
							$state.go('presenca');
							// atualizando cor de check list enviado
							Check.checklist();
							console.log(info, 'CheckList salvo');
						}	
					})
		       }
	    }

		// avançar questão
	    function next(question){
	        addInHistory(question, vm.count);
	        vm.count++;
		
	        //Veririfica se a questão ja possui resposta
	        if(!vm.historyAnswers[vm.count]){
	          vm.answer = null;
	        }else{
	          vm.answer = vm.historyAnswers[vm.count].answer;
	        }

	        changeQuestion();
	        changeTitle();
	    }

		// voltar questão
	    function previous(){
		    vm.count--;
		    vm.answer = vm.historyAnswers[vm.count].answer;
		    changeQuestion();
		    changeTitle();
	    }

	}

})();
