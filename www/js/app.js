// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular
  .module('starter', ['ionic','ionic.service.core','ngCordova', 'fileLogger'])
  .config(function ($stateProvider, $urlRouterProvider){
      $stateProvider
        //Objeto em JavaScript = {}
        
        //definindo rotas das telas e seus controllers
        .state('login', {
          'url': '/login',
          'templateUrl': 'views/login.html',
          'controller': 'loginCtrl',
          'cache': false
        })

        .state('presenca', {
          'url': '/presenca',
          'templateUrl': 'views/presenca.html',
          'controller': 'presencaCtrl',
          'params': {username: null},
          'cache': false
        })

        .state('contato', {
          'url': '/contato/:previousState',
          'templateUrl': 'views/contato.html',
          'controller' : 'sobreCtrl',
          'params' : {previousState: null},
          'cache': false
        })

        .state('fac', {
          'url': '/fac/:previousState',
          'templateUrl': 'views/fac.html',
          'controller' : 'sobreCtrl',
          'params' : {previousState: null},
          'cache': false
        })

        .state('sobre',{
          'url' : '/sobre',
          'templateUrl' : 'views/sobre.html',
          'controller' : 'sobreCtrl',
          'cache': false
        })

        // .state('file',{
        //   'url' : '/file',
        //   'templateUrl' : 'views/file.html',
        //   'controller' : 'fileCtrl',
        //   'cache': false
        // })

        // .state('checkList',{
        //   'url' : '/checkList',
        //   'templateUrl' : 'views/checkList.html',
        //   'controller' : 'checkListCtrl',
        //   'cache': false
        // })

        // .state('checkListDinamico',{
        //   'url' : '/checkListDinamico',
        //   'templateUrl' : 'views/checkListDinamico.html',
        //   'controller' : 'QuestionarioCtrl',
        //   'cache': false
        // })

        .state('checkList',{
          'url' : '/checkList',
          'templateUrl' : 'views/checkListDinamico.html',
          'controller' : 'QuestionarioCtrl',
          'cache': false
        })

        .state('checkInOut',{
          'url' : '/checkInOut',
          'templateUrl' : 'views/checkInOut.html',
          'controller' : 'presencaCtrl',
          'cache': false
        })

        .state('marketing',{
          'url' : '/marketing',
          'templateUrl' : 'views/marketing.html',
          'controller' : 'marketingCtrl',
          'cache': false
        })

        .state('routes',{
          'url' : '/routes',
          'templateUrl' : 'views/routes.html',
          'controller' : 'routesCtrl',
          'cache': false
        })

        .state('images',{
          'url': '/images',
          'templateUrl': 'views/images.html',
          'controller': 'imagesCtrl',
          'cache': false
        })

        .state('solicitacoes',{
          'url': '/solicitacoes',
          'templateUrl': 'views/solicitacoes.html',
          'cache': false
        })

        .state('materiais',{
          'url': '/materiais',
          'templateUrl': 'views/materiais.html',
          'controller': 'materiaisCtrl',
          'cache': false
        })

        .state('uniformes',{
          'url': '/uniformes',
          'templateUrl': 'views/uniformes.html',
          'controller': 'uniformesCtrl',
          'cache': false
        })

        .state('versao',{
          'url': '/versao',
          'templateUrl': 'views/versionFail.html',
          'controller': 'sobreCtrl',
          'cache': false
        })

        .state('termo',{
          'url': '/termo',
          'templateUrl': 'views/termo.html',
          'controller': 'termoCtrl',
          'cache': false
        })

        .state('transferencia',{
          'url': '/transferencia',
          'templateUrl': 'views/transferencia.html',
          'controller': 'transferenciasCtrl',
          'cache': false
        })

        .state('atestado',{
          'url': '/atestado',
          'templateUrl': 'views/atestado.html',
          'controller': 'atestadoCtrl',
          'cache': false
        })

        .state('desbloqueio',{
          'url': '/desbloqueio',
          'templateUrl': 'views/desbloqueio.html',
          'controller': 'desbloqueioCtrl',
          'cache': false
        })

      $urlRouterProvider.otherwise('/login');
      //$ionicHistory.cleanHistory();
    })

// apenas um view por tela é criada
.config(function ($ionicConfigProvider){
    $ionicConfigProvider.views.maxCache(1);
})

.run(function($ionicPlatform, $location, DB, Popup, Check, $ionicPopup, User) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  $ionicPlatform.ready(function() {
    // Enable to debug issues.
    // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
    
    var notificationOpenedCallback = function(jsonData) {
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    window.plugins.OneSignal
      .startInit("3a5efbf7-651b-4c09-85bc-f059e5ec9208")
      .handleNotificationOpened(notificationOpenedCallback)
      .endInit();
      
    // Call syncHashedEmail anywhere in your app if you have the user's email.
    // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
    // window.plugins.OneSignal.syncHashedEmail(userEmail);
  })

  //desabilitando botao Voltar do android
  $ionicPlatform.registerBackButtonAction( function (){
    if(true){
        Popup.sairApp();
        // localStorage = null;
    } else{
      
    }
  }, 100)

  /*$location.url("/login");*/
  DB.initDb();
})
