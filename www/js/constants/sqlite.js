(function() {
	'use strict';
	
	// criação daas tabelas no SQLite
	angular
		.module('starter')
		.constant('SQLITE_CONFIG',{
			'name' : 'dbRhcop',
			'location' : 'default',
			'tables' : {
				'tbFuncionarios' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'nome' : 'TEXT',
					'funcionario' : 'INTEGER',
					'sobrenome' : 'TEXT',
					'email' : 'TEXT',
					'username' : 'TEXT',
					'senha' : 'TEXT',
					'cargo' : 'TEXT',
					'status' : 'TEXT',
					'idChecklist': 'INTEGER'
				},
				'tbMarketing' : {
					'id' : 'INTEGER', 
					'mensagem' : 'TEXT',
					'created_at' : 'TEXT'
				},
				'tbRoutes' : {
					'id' : 'INTEGER',
					'nome' : 'TEXT',
					'funcionario' : 'INTEGER',
					'dia_da_semana' : 'INTEGER',
					'data': 'TEXT'
				},
				'tbCartao_ponto' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'funcionario' : 'INTEGER',
					'checkin' : 'TEXT',
					'checkout' : 'TEXT',
					'cartao_pontocol' : 'TEXT',
					'latitude' : 'TEXT',
					'longitude' : 'TEXT',
					'status' : 'TEXT'
				},
				'tbLogin' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'username' : 'TEXT', 
					'password' : 'TEXT', 
					'dataHora' : 'TEXT', 
					'status' : 'TEXT', 
					'versao' : 'TEXT',
					'uuid' : 'TEXT',
					'model' : 'TEXT',
					'versionDevice' : 'TEXT'
				},
				'tbDebug' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'mensagem' : 'TEXT',
					'erro' : 'TEXT',
					'dataHora' : 'TEXT'
				},
				'tbTermo' : {
					'id' : 'INTEGER',
					'status' : 'INTEGER'
				},
				'tbTermoOffline' :{
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'funcionario' : 'INTEGER',
					'dataHora' : 'TEXT',
					'status' : 'TEXT',
					'versao' : 'TEXT'
				},
				'tbMarcacao' : {
					'id' : 'INTEGER',
					'status': 'TEXT'
				},
				'tbConfiguracao' : {
					'id' : 'INTEGER',
					'lojaFunc' : 'TEXT',
					'lojaLat' : 'TEXT',
					'lojaLong' : 'TEXT',
					'gpsAtivo' : 'INTEGER',
					'metrosAtivo' : 'INTEGER',
					'cargoQuinhentos' : 'INTEGER'
				},
				'tbCheckListDinamico' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'idChecklist' : 'INTEGER',
					'funcionario' : 'INTEGER',
					'checklist': 'TEXT'
				},
				'tbCheckListDinamicoResposta' : {
					'id' : 'INTEGER PRIMARY KEY AUTOINCREMENT',
					'funcionario' : 'INTEGER',
					'checklist': 'TEXT',
					'dataHora': 'TEXT',
					'loja': 'INTEGER'
				}
			}
		}) 
})();
