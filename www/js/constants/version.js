(function () {
    'use strict';

    angular
        .module('starter')
        .constant('VERSION', {
            name: '',
            value: '5.8.21',
            date: '21/08/2018'
        })
})();